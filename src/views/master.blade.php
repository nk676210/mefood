<html>
    <head>
        <title>App Name - @yield('title')</title>
    </head>
    <body>
		@include('MeFood::partials.navigation')
		<div clas="container">
			@yield('content')
		</div>
    </body>
</html>
