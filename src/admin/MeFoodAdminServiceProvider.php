<?php

namespace DatumCore\MeFood\Admin;

use Illuminate\Support\ServiceProvider;

class MeFoodAdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->make(MeFoodAdminController::class);

        include(__DIR__ . '/routes.php');
    
        $this->loadViewsFrom(__DIR__ . '/views', 'MeFoodAdmin');

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
