<?php

namespace DatumCore\MeFood;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MeFoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $food = new \DatumCore\MeFood\Food;
		$message = $food->get_table_name();

        return view('MeFood::welcome', compact('message'));
    }
	
    public function content()
    {
        return view('MeFood::content');
    }	
}
