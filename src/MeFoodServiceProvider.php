<?php

namespace DatumCore\MeFood;

use Illuminate\Support\ServiceProvider;

class MeFoodServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->make(MeFoodController::class);

        include(__DIR__ . '/routes.php');
    
        $this->loadViewsFrom(__DIR__ . '/views', 'MeFood');

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
