<?php

namespace DatumCore\MeFood\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MeFoodAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $message = 'aaaaaa';
        return view('MeFoodAdmin::welcome', compact('message'));
    }
}
