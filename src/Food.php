<?php

namespace DatumCore\MeFood;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    public function get_table_name()
    {
        return 'food_table';
    }
}
